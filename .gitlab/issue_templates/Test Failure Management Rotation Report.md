<!--
The issue title should include the phrase "Test Failure Management" and the week's start and end date, e.g.:

  Test Failure Management 2019-11-04 to 2019-11-10

The issue description should outline the state of the pipeline/environments and briefly mention any highlights, e.g., application bugs found, noteworthy infrastructure issues, or changes that affected multiple tests, etc.
-->

# Summary

## Highlights

## Master :white_check_mark: / :x:

## Nightly :white_check_mark: / :x:

## Production :white_check_mark: / :x:

## Staging :white_check_mark: / :x:

<!--
The discussions can be organized however you would like, as long as they help you keep track of the state of the tests.

For example, you could start one discussion each day in which you list the tests that fail. You could edit the same comment to update the list, or you could add new failures as new comments.

Alternatively, you could start a discussion for each environment/pipeline, or even a separate discussion for each test.
-->